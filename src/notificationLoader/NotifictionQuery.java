package notificationLoader;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import org.apache.http.HttpEntity;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.os.AsyncTask;
import android.util.Log;

import com.kadtech.xlabx.typo.Notifications;

public class NotifictionQuery extends AsyncTask<String, Void, String>{

	private static String result;
	private static int id;
	private static  String queryUrl ;
	public NotifictionQuery(int getterId){
		id = getterId;
		queryUrl = "http://www.typoday.in/notifications.php?q=";
	}
	
	@Override
	protected String doInBackground(String... params) {
		HttpClient httpClient = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(NotifictionQuery.queryUrl+id);
		Log.i("query", queryUrl+id);
		try {
			HttpEntity httpEntity = httpClient.execute(httpGet).getEntity();
			if (httpEntity != null) {
				InputStream inputStream = httpEntity.getContent();
				Reader in = new InputStreamReader(inputStream);
				BufferedReader bufferedreader = new BufferedReader(in);
				StringBuilder stringBuilder = new StringBuilder();

				String stringReadLine = null;

				while ((stringReadLine = bufferedreader.readLine()) != null) {
					stringBuilder.append(stringReadLine);
				}

				result = stringBuilder.toString();
			}

		} catch (Exception e) {
			e.printStackTrace();
		} catch (Error e){
			e.printStackTrace();
		}
		return result;
	}
	
	@Override
	protected void onPostExecute(String result) {
		Notifications.uiUpdate(result);
		super.onPostExecute(result);
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
	}

}
