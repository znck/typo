package com.kadtech.xlabx.typo;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

public class Disclaimer extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_disclaimer);
	}
	public void onClick(View v){
		this.finish();
	}
}
