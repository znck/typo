package com.kadtech.xlabx.typo;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class Speaker_1 extends Activity implements OnClickListener{
	public static boolean menu_vis = false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_speaker_1);
		Dropdown.action_menu = (View)this.findViewById(R.id.scrollView2);
		Dropdown.menu_button = (View)this.findViewById(R.id.buttton_menu);
		Dropdown.delay = new Handler();
		TextView tv = ((TextView)this.findViewById(R.id.weblink_speaker_1));
		tv.setText(Html.fromHtml("+ <a href=\""+this.getResources().getString(R.string.speaker_1_url)+"\">read more</a>"));
		tv.setMovementMethod(LinkMovementMethod.getInstance());
	}

	@Override
	public void onResume()
	{
		super.onResume();
		Dropdown.action_menu = (View)this.findViewById(R.id.scrollView2);
		Dropdown.menu_button = (View)this.findViewById(R.id.buttton_menu);
		Dropdown.delay = new Handler();
	}
	
	@Override
	public void onClick(View v) {
		switch(v.getId()){
				case R.id.buttton_menu: menu_vis = Dropdown.Drop(menu_vis); break;
				case R.id.button_back: this.finish(); break;
				case R.id.home_menu:
				case R.id.about_us_menu:
				case R.id.contact_menu:
				case R.id.workshops_menu:
				case R.id.results_menu:
				case R.id.lodging_menu:
				case R.id.travel_menu:
				case R.id.weather_menu:
				case R.id.speakers_menu: ActionBar.setClick(v); this.setResult(1); this.finish(); break;
			}
		
	}

}
