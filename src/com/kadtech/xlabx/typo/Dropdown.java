package com.kadtech.xlabx.typo;

import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LayoutAnimationController;
import android.view.animation.TranslateAnimation;

public class Dropdown {
	public static View action_menu, menu_button;
	public static Handler delay;
	public static Context context;
	public static boolean Drop(boolean menu_vis)
	{
		if(menu_vis)
		{
			menu_vis = false;
			AnimationSet set = new AnimationSet(true);
			
		    Animation animation = new AlphaAnimation(0.0f, 1.0f);
		    animation.setDuration(0);
		    set.addAnimation(animation);

		    animation = new TranslateAnimation(
		          Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
		          Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, -1.0f
		    );
		    animation.setDuration(500);
		    //animation.setAnimationListener(al);
		    set.addAnimation(animation);									    

		    LayoutAnimationController controller =
		    new LayoutAnimationController(set, 0.25f);
		    ((ViewGroup) action_menu).setLayoutAnimation(controller);
		    delay.postDelayed(new Runnable(){

				@Override
				public void run() {
					action_menu.setVisibility(View.GONE);												
				}	}, 500);
		}
		else
		{
			action_menu.setVisibility(View.VISIBLE);
			menu_vis = true;
			//menu_button.setBackgroundColor(this.getResources().getColor(R.color.Black));
			AnimationSet set = new AnimationSet(true);
			
		    Animation animation = new AlphaAnimation(0.0f, 1.0f);
		    animation.setDuration(100);
		    set.addAnimation(animation);

		    animation = new TranslateAnimation(
		          Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
		          Animation.RELATIVE_TO_SELF, -1.0f, Animation.RELATIVE_TO_SELF, 0.0f
		    );
		    animation.setDuration(500);
		    set.addAnimation(animation);

		    LayoutAnimationController controller =
		    new LayoutAnimationController(set, 0.25f);
		    ((ViewGroup) action_menu).setLayoutAnimation(controller);
		}
		return menu_vis;
	}
}
