package com.kadtech.xlabx.typo;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.CalendarContract;
import android.provider.CalendarContract.Events;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Schedule extends Activity implements OnClickListener, OnCheckedChangeListener{
	public static boolean menu_vis = false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_schedule);
		Dropdown.action_menu = (View)this.findViewById(R.id.scrollView2);
		Dropdown.menu_button = (View)this.findViewById(R.id.buttton_menu);
		Dropdown.delay = new Handler();
		this.setResult(0);
		setContent();
	}

	@Override
	public void onResume()
	{
		super.onResume();
		Dropdown.action_menu = (View)this.findViewById(R.id.scrollView2);
		Dropdown.menu_button = (View)this.findViewById(R.id.buttton_menu);
		Dropdown.delay = new Handler();
	}

	@Override
	public void onClick(View v) {
		switch(v.getId())
		{
			case R.id.buttton_menu: menu_vis = Dropdown.Drop(menu_vis); break;
			case R.id.button_back:
			case R.id.home_menu: this.finish(); break;
			case R.id.about_us_menu:
			case R.id.contact_menu:
			case R.id.workshops_menu:
			case R.id.results_menu:
			case R.id.lodging_menu:
			case R.id.travel_menu:
			case R.id.weather_menu:
			case R.id.speakers_menu: ActionBar.setClick(v); this.setResult(1); this.finish(); break;
			case R.id.llday1but: show(1); break;
			case R.id.llday2but: show(2); break;
			case R.id.llday3but: show(3); break;
		}
	}
	private int visible = 1;
	private String x,y;
	private void show(int i) {
		if(i==visible)return;
		this.getView("schdule_daypar"+i).setVisibility(View.VISIBLE);
		((TextView) ((ViewGroup) this.getView("llday"+i+"but")).getChildAt(0)).setTextColor(this.getResources().getColor(R.color.White));
		((TextView) ((ViewGroup) this.getView("llday"+i+"but")).getChildAt(1)).setTextColor(this.getResources().getColor(R.color.White));
		((TextView) ((ViewGroup) this.getView("llday"+i+"but")).getChildAt(1)).setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, R.drawable.black);
		
		this.getView("schdule_daypar"+visible).setVisibility(View.GONE);
		((TextView) ((ViewGroup) this.getView("llday"+visible+"but")).getChildAt(0)).setTextColor(this.getResources().getColor(R.color.col));
		((TextView) ((ViewGroup) this.getView("llday"+visible+"but")).getChildAt(1)).setTextColor(this.getResources().getColor(R.color.col));
		((TextView) ((ViewGroup) this.getView("llday"+visible+"but")).getChildAt(1)).setCompoundDrawables(null,null,null,null);
		visible = i;
	}
	
	public View getView(String id){
		return this.findViewById(this.getId(id, "id"));
	}

	public int getId(String name, String defType){
		return this.getResources().getIdentifier(name, defType, this.getPackageName());
	}
	private void setContent(){
		for( int m = 1; m <= 3; ++m){
		LinearLayout container = (LinearLayout) this.findViewById(this.getId("schdule_day"+m, "id")),
				event, data;
		TextView time, name,link;
		ImageView sep;
		LinearLayout.LayoutParams wrap = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		LinearLayout.LayoutParams wrap2 = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		LinearLayout.LayoutParams fill = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		LinearLayout.LayoutParams full = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
		full.setMargins(15, 4, 15, 4);
		full.gravity = Gravity.CENTER;
		fill.setMargins(8, 8, 8, 0);
		fill.gravity = Gravity.CENTER;
		LinearLayout.LayoutParams parent = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		CheckBox notif;
		Database_Handler db = new Database_Handler();
		Cursor cur = db.dr.rawQuery("SELECT * FROM "+Database.schedule+" WHERE "+Database.s_on+"="+m, null);
		try{
		cur.moveToFirst();
		int i = cur.getCount();
		while(i-- > 0){
			event = new LinearLayout(this);
			data = new LinearLayout(this);
			time = new TextView(this);
			name = new TextView(this);
			link = new TextView(this);
			notif = new CheckBox(this);
			sep = new ImageView(this);
			sep.setLayoutParams(full);
			event.setLayoutParams(fill);
			data.setLayoutParams(parent);
			time.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT));
			name.setLayoutParams(wrap);
			link.setLayoutParams(wrap);
			//name.setGravity(Gravity.CENTER);
			//link.setGravity(Gravity.CENTER);
			
			sep.setBackgroundResource(R.drawable.red_vertical);
			
			event.setBackgroundResource(R.drawable.round_white);
			event.setPadding(16,16,16,16);
			
			time.setPadding(20, 0, 0, 0);
			
			data.addView(name);
			data.addView(link);
			
			wrap2.gravity = Gravity.RIGHT;
			notif.setLayoutParams(wrap2);
			
			data.addView(notif);
			
			event.addView(time);
			event.addView(sep);
			event.addView(data);
			
			container.addView(event);
			
			event.setOrientation(LinearLayout.HORIZONTAL);
			data.setOrientation(LinearLayout.VERTICAL);
			
			/////////////////////////
			time.setText(Html.fromHtml("<b>"+Xdate.get( cur.getString( cur.getColumnIndex(Database.s_start) ) ).format("HH:mm") + "<br />-<br />"+ Xdate.get( cur.getString( cur.getColumnIndex(Database.s_end) ) ).format("HH:mm")+"</b>"));
			time.setTextColor(this.getResources().getColor(R.color.Black));
			time.setGravity(Gravity.CENTER);
			name.setText(Html.fromHtml("<b>"+cur.getString(cur.getColumnIndex(Database.s_event))+"</b>"));
			name.setTextColor(this.getResources().getColor(R.color.Black));
			
			if(cur.getString(cur.getColumnIndex(Database.s_blank)).length() > 1){
				link.setText(cur.getString(cur.getColumnIndex(Database.s_blank)));
				link.setTextColor(this.getResources().getColor(R.color.red));
				x = cur.getString(cur.getColumnIndex(Database.s_extra));
				y = cur.getString(cur.getColumnIndex(Database.s_event));
				link.setOnClickListener(new OnClickListener(){
					String x = Schedule.this.x;
					String c = Schedule.this.y;
					@Override
					public void onClick(View v) {
						Intent intent = new Intent(Schedule.this.getApplicationContext(),Popup.class);
						intent.putExtra("title", c);
						intent.putExtra("content", x);
						Schedule.this.startActivity(intent);
					}
					
				});
				name.setOnClickListener(new OnClickListener(){
					String x = Schedule.this.x;
					String c = Schedule.this.y;
					@Override
					public void onClick(View v) {
						Intent intent = new Intent(Schedule.this.getApplicationContext(),Popup.class);
						intent.putExtra("title", c);
						intent.putExtra("content", x);
						Schedule.this.startActivity(intent);
					}
					
				});
			}
			notif.setText("Add to calendar");
			notif.setTextColor(this.getResources().getColor(R.color.Black));
			notif.setTextSize(14);
			if(Boolean.parseBoolean(cur.getString(cur.getColumnIndex(Database.s_notif)))){
				notif.setChecked(true);
			}
			notif.setTag(cur.getInt(cur.getColumnIndex(Database.id)));
			notif.setOnCheckedChangeListener(this);
			cur.moveToNext();
		}
		cur.close();
		db.close();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(db!=null){
				try{
					cur.close();
					db.close();
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		}
		}
	}

	@Override
	public void onCheckedChanged(CompoundButton v, boolean isChecked) throws ActivityNotFoundException {
		Database_Handler db = new Database_Handler();
		if(isChecked){
			Uri uri = null;
			Cursor cur = db.dr.rawQuery("SELECT * FROM "+Database.schedule+" WHERE id="+v.getTag(), null);
			cur.moveToFirst();
			long id;
			Xdate sd = Xdate.get(cur.getString(cur.getColumnIndex(Database.s_start))), ed = Xdate.get(cur.getString(cur.getColumnIndex(Database.s_end)));
			id = cur.getInt(cur.getColumnIndex(Database.id));
			String loc = "Department of Design, IIT Guwahati";
				if(cur.getInt(cur.getColumnIndex(Database.s_on))!=1){
					loc = "New Conference Hall, IIT Guwahati";
				}
				
			Intent intent = new Intent();
			String insertId = "";
			boolean launch = true, failed = false;
			try{
				intent = new Intent(Intent.ACTION_INSERT)
	        .setData(Events.CONTENT_URI)
	        	.setType("vnd.android.cursor.item/event")
		        .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, sd.milis())
		        .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, ed.milis())
		        .putExtra(Events.TITLE, "Typoday: "+cur.getString(cur.getColumnIndex(Database.s_event)))
		        .putExtra(Events.DESCRIPTION, Html.fromHtml(cur.getString(cur.getColumnIndex(Database.s_extra)).replaceAll("###", "\n")).toString())
		        .putExtra(Events.EVENT_LOCATION, loc)
		        .putExtra(Events.CALENDAR_ID, 1)
		        .putExtra(Events.HAS_ALARM, 1)
		        .putExtra(Events.AVAILABILITY, Events.AVAILABILITY_BUSY);
			}catch(Exception e){
				e.printStackTrace();
			}
			catch(Error e){
				launch = false;
				e.printStackTrace();
				try{
					Log.i("error", "isert");
					ContentResolver cr = this.getContentResolver();
					ContentValues values = new ContentValues();
					values.put("calendar_id", 1);
					values.put("dtstart", sd.milis());
					values.put("dtend", ed.milis());
					values.put("eventLocation", loc);
					values.put("hasAlarm", 1);
					values.put("title", "Typoday: "+cur.getString(cur.getColumnIndex(Database.s_event)));
					values.put("description", Html.fromHtml(cur.getString(cur.getColumnIndex(Database.s_extra)).replaceAll("###", "\n")).toString());
					uri = cr.insert(Uri.parse("content://com.android.calendar/events"), values);
					Log.v("sssssssssssssEc", uri.toString());
					insertId = uri.toString();
					
					Log.i("r", "mi");
					if(Long.parseLong(uri.getLastPathSegment()) != 0){
						Toast.makeText(this, "Event added", Toast.LENGTH_SHORT).show();
					}
				}catch(Exception ex){
					failed = true;
					ex.printStackTrace();
					Toast.makeText(this, "Failed to add event", Toast.LENGTH_SHORT).show();
				}catch(Error ex){
					failed = true;
					ex.printStackTrace();
					Toast.makeText(this, "Failed to add event", Toast.LENGTH_SHORT).show();
				}
			}finally{
			cur.close();
			// Updating value
			if(!failed || launch){
			ContentValues cv = new ContentValues();
			cv.put(Database.s_notif, "true "+insertId);
			db.dr.update(Database.schedule, cv, "id="+id, null);
			}else{
				v.setChecked(false);
			}
			db.close();
			if(launch)
				this.startActivityForResult(intent,-1);
			}
		}else{
			try{
				Cursor cur = db.dr.rawQuery("SELECT * FROM "+Database.schedule+" WHERE id="+v.getTag(), null);
				cur.moveToFirst();
				ContentResolver cr = this.getContentResolver();
				Uri delUri = Uri.parse(cur.getString(cur.getColumnIndex(Database.s_notif)).split(" ")[1]);
				int rows = cr.delete(delUri, null, null);
				if( rows > 0)
					Toast.makeText(this, "Removed from calendar", Toast.LENGTH_SHORT).show();
				Log.i("Debuggind", "row deleted: "+rows);
				
				ContentValues cv = new ContentValues();
				cv.put(Database.s_notif, "false");
				db.dr.update(Database.schedule, cv, "id="+Integer.parseInt(v.getTag().toString()), null);
			}catch(Exception e){
				e.printStackTrace();
				Toast.makeText(this, "No permissions. Try deleting it manually.", Toast.LENGTH_SHORT).show();
				v.setChecked(true);
			}catch(Error e){
				e.printStackTrace();
				Toast.makeText(this, "Failed to delete event. Try deleting it manually.", Toast.LENGTH_SHORT).show();
			}
			db.close();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		Log.v("sdfsfsdfsd", resultCode+" "+data.toString());
	}
}
