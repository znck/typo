package com.kadtech.xlabx.typo;


import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Database_Handler extends SQLiteOpenHelper{
	
	public SQLiteDatabase dr;
	public SQLiteDatabase dw;
	public Database d = new Database();

	public Database_Handler() {
		super(Main.context, Database.NAME, null, Database.VERSION);
		dr = this.getReadableDatabase();
		dw = this.getWritableDatabase();
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		d.create(database);
	}

	@Override
	public void onUpgrade(SQLiteDatabase database, int old_version, int new_version) {
		d.delete(database);
		d.create(database);		
	}
	
	public Cursor getHotels(String orderby)
	{
		String query = "SELECT * FROM "+Database.places+" ORDER BY "+orderby+" DESC";
		return dr.rawQuery(query, null);
	}

	public Cursor getHotel(int id) {
		String query = "SELECT * FROM "+Database.places+" WHERE id="+id;
		return dr.rawQuery(query, null);
	}
	
	public Cursor getPlaceLetters(){
		String query = "SELECT DISTINCT substr("+Database.g_name+",1,1) AS let FROM "+Database.gps+" ORDER BY "+Database.g_name;
		return dr.rawQuery(query, null);
	}

	public Cursor getPlace() {
		String query = "SELECT * FROM "+Database.gps+" ORDER BY "+Database.g_name;
		return dr.rawQuery(query, null);
	}
	
	public Cursor getWeather(){
		String query = "SELECT * FROM "+Database.weather;
		return dr.rawQuery(query, null);
	}
	public void clearWeather(){
		dw.execSQL("DELETE FROM "+Database.weather);
	}
	
	public Cursor getWorkshops(){
		return dr.rawQuery("SELECT * FROM "+Database.workshops+" ORDER BY "+Database.x_name, null);
	}
	
	public Cursor getWorkshops(int id){
		return dr.rawQuery("SELECT * FROM "+Database.workshops+" WHERE `id`="+id, null);
	}
}
