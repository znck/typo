package com.kadtech.xlabx.typo;

import android.view.View;

public class ActionBar {
	public static View cache = null;
	public static void setClick(View v)
	{
		cache = v;
	}
	public static View getClick()
	{
		View copy = cache;
		cache = null;
		return copy;
	}
	public static boolean isClickSet()
	{
		return (cache == null)? false: true;
	}
	public static boolean clicked(View v, boolean menu_vis){
		boolean ret = false;
		switch(v.getId()){
			// Action bar menu button
			case R.id.buttton_menu: Dropdown.Drop(menu_vis); ret=true; break; 
		}
		return ret;
	}
}
