package com.kadtech.xlabx.typo;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class Xdate{
	static Xdate d;
	Date date;
	SimpleDateFormat sdf;
	private Xdate(){}
	public static Xdate get(String s){
		d = new Xdate();
		d.sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
		try{
			d.date = d.sdf.parse(s);
		}catch(Exception e){
			e.printStackTrace();
		}
		return d;
	}
	public String format(String s){
		sdf = new SimpleDateFormat(s, Locale.US);
		return sdf.format(date);
	}
	public long milis(){
		return this.date.getTime();
	}
}
