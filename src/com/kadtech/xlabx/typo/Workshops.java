package com.kadtech.xlabx.typo;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class Workshops extends Activity implements OnClickListener {
	public static boolean menu_vis = false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_workshops);
		Dropdown.action_menu = (View)this.findViewById(R.id.scrollView2);
		Dropdown.menu_button = (View)this.findViewById(R.id.buttton_menu);
		Dropdown.delay = new Handler();
		this.setResult(0);
		this.load();
	}

	public void load(){
		LinearLayout target = (LinearLayout) ((ViewGroup) this.findViewById(R.id.scrollView1)).getChildAt(0);
		Database_Handler db = new Database_Handler();
		Cursor cur = db.getWorkshops();
		cur.moveToFirst();
		ImageView view;
		for( int i = 0; i < cur.getCount(); i++){
			if(cur.getString(cur.getColumnIndex(Database.x_image)).split(";").length > 1){
				String images[] = cur.getString(cur.getColumnIndex(Database.x_image)).split(";");
				
				view  = new ImageView(this);
				view.setImageResource(this.getId(images[0]+"1", "drawable"));
				view.setOnClickListener(this);
				view.setTag(cur.getInt(cur.getColumnIndex(Database.id)));
				target.addView(view);
				
				view  = new ImageView(this);
				view.setImageResource(this.getId(images[1]+"1", "drawable"));
				view.setOnClickListener(this);
				view.setTag(cur.getInt(cur.getColumnIndex(Database.id)));
				target.addView(view);
			}else{
				view  = new ImageView(this);
				view.setImageResource(this.getId(cur.getString(cur.getColumnIndex(Database.x_image))+"1", "drawable"));
				view.setOnClickListener(this);
				view.setTag(cur.getInt(cur.getColumnIndex(Database.id)));
				target.addView(view);
			}
			cur.moveToNext();
		}
		cur.close();
		db.close();
	}
	
	public int getId(String name, String defType){
		return this.getResources().getIdentifier(name, defType, this.getPackageName());
	}
	
	@Override
	public void onResume()
	{
		super.onResume();
		Dropdown.action_menu = (View)this.findViewById(R.id.scrollView2);
		Dropdown.menu_button = (View)this.findViewById(R.id.buttton_menu);
		Dropdown.delay = new Handler();
	}

	@Override
	public void onClick(View v) {
		switch(v.getId())
		{
			case R.id.buttton_menu: menu_vis = Dropdown.Drop(menu_vis); break;
			case R.id.button_back:
			case R.id.home_menu: this.finish(); break;
			case R.id.about_us_menu:
			case R.id.contact_menu:
			case R.id.workshops_menu:
			case R.id.results_menu:
			case R.id.lodging_menu:
			case R.id.travel_menu:
			case R.id.weather_menu:
			case R.id.speakers_menu: ActionBar.setClick(v); this.setResult(1); this.finish(); break;
			default:	if(v.getTag() != null){
							Intent intent = new Intent(this, Workshopper.class);
							intent.putExtra("id", (Integer)v.getTag());
							this.startActivityForResult(intent,0);
						}
		}
	}
	@Override
	protected void onActivityResult(int request, int result, Intent data)
	{
		super.onActivityResult(request, result, data);
		if(ActionBar.isClickSet()){
			this.onClick(ActionBar.getClick());
		}
	}
}
