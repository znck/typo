package com.kadtech.xlabx.typo;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import org.apache.http.HttpEntity;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class Main extends Activity implements OnClickListener{
	public static Context context;
	public static boolean menu_vis = false;
	public static int countNot =-1;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		this.setResult(1);
		context = this.getApplicationContext();
		Dropdown.action_menu = (View)this.findViewById(R.id.scrollView2);
		Dropdown.menu_button = (View)this.findViewById(R.id.buttton_menu);
		Dropdown.delay = new Handler();
	}
	public void onResume()
	{
		super.onResume();
		notifications();
		Dropdown.action_menu = (View)this.findViewById(R.id.scrollView2);
		Dropdown.menu_button = (View)this.findViewById(R.id.buttton_menu);
		Dropdown.delay = new Handler();
	}


	@Override
	public void onDestroy()
	{
		super.onDestroy();
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
			case R.id.buttton_menu: menu_vis = Dropdown.Drop(menu_vis); break;
			case R.id.but_speakers:
			case R.id.speakers_menu: this.startActivityForResult(new Intent(Main.context, Speakers.class), 0);
										if(menu_vis){
											Dropdown.action_menu.setVisibility(View.INVISIBLE);
											menu_vis = false;
										}break;
			case R.id.but_results:
			case R.id.results_menu: this.startActivityForResult(new Intent(Main.context, Results.class), 0);
									if(menu_vis){
										Dropdown.action_menu.setVisibility(View.INVISIBLE);
										menu_vis = false;
									}break;
			case R.id.but_contact:
			case R.id.contact_menu:
			case R.id.contact_menu_img: this.startActivityForResult(new Intent(Main.context, Contact.class), 0);
										if(menu_vis){
											Dropdown.action_menu.setVisibility(View.INVISIBLE);
											menu_vis = false;
										}break;
			case R.id.but_lodging:
			case R.id.lodging_menu: this.startActivityForResult(new Intent(Main.context, Lodging.class), 0);
										if(menu_vis){
											Dropdown.action_menu.setVisibility(View.INVISIBLE);
											menu_vis = false;
										}break;
			case R.id.but_about_us:
			case R.id.about_us_menu: this.startActivityForResult(new Intent(Main.context, AboutUs.class), 0);
										if(menu_vis){
											Dropdown.action_menu.setVisibility(View.INVISIBLE);
											menu_vis = false;
										}break;
			case R.id.but_weather:
			case R.id.weather_menu:  this.startActivityForResult(new Intent(Main.context, WeatherAct.class), 0);
										if(menu_vis){
											Dropdown.action_menu.setVisibility(View.INVISIBLE);
											menu_vis = false;
										}break;
										
			case R.id.but_workshops:
			case R.id.workshops_menu:  this.startActivityForResult(new Intent(Main.context, Workshops.class), 0);
										if(menu_vis){
											Dropdown.action_menu.setVisibility(View.INVISIBLE);
											menu_vis = false;
										}break;
			
			case R.id.directions:  this.startActivityForResult(new Intent(Main.context, Directoins.class), 0);
										if(menu_vis){
											Dropdown.action_menu.setVisibility(View.INVISIBLE);
											menu_vis = false;
										}break;
			case R.id.schedule:  this.startActivityForResult(new Intent(Main.context, Schedule.class), 0);
										if(menu_vis){
											Dropdown.action_menu.setVisibility(View.INVISIBLE);
											menu_vis = false;
										}break;
			case R.id.notifications:  ((TextView) this.findViewById(R.id.not_counter)).setText("");
										this.startActivityForResult(new Intent(Main.context, Notifications.class), 0);
											if(menu_vis){
												Dropdown.action_menu.setVisibility(View.INVISIBLE);
												menu_vis = false;
											}break;
			case R.id.but_travel:
			case R.id.travel_menu:  this.startActivityForResult(new Intent(Main.context, Travel.class), 0);
											if(menu_vis){
												Dropdown.action_menu.setVisibility(View.INVISIBLE);
												menu_vis = false;
											}break;
		}
	}
	
	@Override
	protected void onActivityResult(int request, int result, Intent data)
	{
		super.onActivityResult(request, result, data);
		if(ActionBar.isClickSet()){
			onClick((View)this.findViewById(ActionBar.getClick().getId()));
		}
	}
	
	class NotificationCounter extends AsyncTask<String,Void, String>{
		public int id;
		public String result = null;
		public NotificationCounter(int idno){
			id = idno;
		}
		
		@Override
		protected String doInBackground(String... params) {
			HttpClient httpClient = new DefaultHttpClient();
			HttpGet httpGet = new HttpGet("http://www.typoday.in/notifications.php?qc="+id);
			try {
				HttpEntity httpEntity = httpClient.execute(httpGet).getEntity();
				if (httpEntity != null) {
					InputStream inputStream = httpEntity.getContent();
					Reader in = new InputStreamReader(inputStream);
					BufferedReader bufferedreader = new BufferedReader(in);
					StringBuilder stringBuilder = new StringBuilder();

					String stringReadLine = null;

					while ((stringReadLine = bufferedreader.readLine()) != null) {
						stringBuilder.append(stringReadLine);
					}

					result = stringBuilder.toString();
				}

			} catch (Exception e) {
				e.printStackTrace();
			} catch (Error e){
				e.printStackTrace();
			}
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			if(result != null)
			{
				int ret = 0;
				try{
					ret = Integer.parseInt(result);
				}catch(Exception e){
					e.printStackTrace();
				}
				Main.this.addPlus(ret);
			}
			super.onPostExecute(result);
		}
	}

	public void notifications(){
		Database_Handler db = new Database_Handler();
		Cursor cur = db.dr.rawQuery("SELECT COUNT(*) FROM "+Database.updates, null);
		cur.moveToFirst();
		this.findViewById(R.id.not_counter).setTag(cur.getInt(0));
		Log.i("sent", cur.getInt(0)+"ok");
		NotificationCounter nc = new NotificationCounter(cur.getInt(0));
		cur.close();
		db.close();
		nc.execute("");
	}
	public void addPlus(int i) {
		Log.i("got", i+"ok");
		if(i > Integer.parseInt(this.findViewById(R.id.not_counter).getTag().toString())){
			((TextView) this.findViewById(R.id.not_counter)).setText("+");
		}
			countNot = i;
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getItemId() == R.id.menu_about_ap){
			Intent intent = new Intent(this,Popup.class);
			intent.putExtra("title", "About App");
			intent.putExtra("content", "<h1 align=\"center\">Typoday 2013</h1>Designer: &nbsp;<a href=\"http://www.himanshuseth.com\">Himanshu Seth</a><br />Developer: <a href=\"http://www.rahulkadyan.com/\">Rahul Kadyan</a>");
			this.startActivity(intent);
		}
		return super.onOptionsItemSelected(item);
	}
}
