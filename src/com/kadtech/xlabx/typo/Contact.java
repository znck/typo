package com.kadtech.xlabx.typo;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.text.Spannable;
import android.text.style.UnderlineSpan;
import android.text.util.Linkify;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class Contact extends Activity implements OnClickListener{
	public static boolean menu_vis = false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_contact);
		Dropdown.action_menu = (View)this.findViewById(R.id.scrollView2);
		Dropdown.menu_button = (View)this.findViewById(R.id.buttton_menu);
		Dropdown.delay = new Handler();
		TextView tv = (TextView)this.findViewById(R.id.contact_email);
//		SpannableStringBuilder ssb = new SpannableStringBuilder();
//		ssb.append(this.getResources().getString(R.string.contact_email));
//		ssb.setSpan(new TextLink(), 0, tv.getText().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//		tv.setMovementMethod(LinkMovementMethod.getInstance());
//		tv.setText(ssb);
//		SpannableString ss = new SpannableString(tv.getText());
		Linkify.addLinks(tv, Linkify.EMAIL_ADDRESSES);
		Spannable s = (Spannable) tv.getText();
		UnderlineSpan us = new UnderlineSpan();
		s.removeSpan(us);
//		tv.setLinkTextColor(this.getResources().getColor(R.color.White));
	}

	@Override
	public void onResume()
	{
		super.onResume();
		Dropdown.action_menu = (View)this.findViewById(R.id.scrollView2);
		Dropdown.menu_button = (View)this.findViewById(R.id.buttton_menu);
		Dropdown.delay = new Handler();
	}

	@Override
	public void onClick(View v) {
		switch(v.getId())
		{
			case R.id.buttton_menu: menu_vis = Dropdown.Drop(menu_vis); break;
			case R.id.button_back:
			case R.id.home_menu: this.finish(); break;
			case R.id.about_us_menu:
			case R.id.contact_menu:
			case R.id.workshops_menu:
			case R.id.results_menu:
			case R.id.lodging_menu:
			case R.id.travel_menu:
			case R.id.weather_menu:
			case R.id.speakers_menu: ActionBar.setClick(v); this.setResult(1); this.finish();
		}
	}
}
