package com.kadtech.xlabx.typo;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

public class Directoins extends Activity implements OnClickListener, OnCheckedChangeListener, OnEditorActionListener{
	public static boolean menu_vis = false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_directoins);
		Dropdown.action_menu = (View)this.findViewById(R.id.scrollView2);
		Dropdown.menu_button = (View)this.findViewById(R.id.buttton_menu);
		Dropdown.delay = new Handler();
		this.setResult(0);
		((CompoundButton) this.findViewById(R.id.set_destination_as_current)).setOnCheckedChangeListener(this);
		((CompoundButton) this.findViewById(R.id.set_source_as_current)).setOnCheckedChangeListener(this);
		((TextView) findViewById(R.id.source)).setOnEditorActionListener(this);
		((TextView) findViewById(R.id.destination)).setOnEditorActionListener(this);
	}

	@Override
	public void onClick(View v) {
		switch(v.getId())
		{
			case R.id.buttton_menu: menu_vis = Dropdown.Drop(menu_vis); break;
			case R.id.button_back:
			case R.id.home_menu: this.finish(); break;
			case R.id.about_us_menu:
			case R.id.contact_menu:
			case R.id.workshops_menu:
			case R.id.results_menu:
			case R.id.lodging_menu:
			case R.id.travel_menu:
			case R.id.weather_menu:
			case R.id.speakers_menu: ActionBar.setClick(v); this.setResult(1); this.finish(); break;
			case R.id.set_source: Intent intent = new Intent(this,Select_place.class);
									intent.putExtra("title", "Select Source");
									this.startActivityForResult(intent, 1); break;
			case R.id.set_destination: Intent inten = new Intent(this,Select_place.class);
										inten.putExtra("title", "Select Destination");
										this.startActivityForResult(inten, 2); break;
			case R.id.route: if(this.findViewById(R.id.source).getTag() == null){ 
									if( ((EditText) this.findViewById(R.id.source)).getText() == null || ((EditText) this.findViewById(R.id.source)).getText().toString() == "" ){
											Toast.makeText(this, "Source not set.", Toast.LENGTH_SHORT).show(); break;
											} else{ 
												this.findViewById(R.id.source).setTag(( ((EditText) this.findViewById(R.id.source)).getText()).toString().replaceAll(" ","+")); 
											} 
									}
							if(this.findViewById(R.id.destination).getTag() == null){ 
								if( ((EditText) this.findViewById(R.id.destination)).getText() == null || ((EditText) this.findViewById(R.id.destination)).getText().toString() == "" ){
										Toast.makeText(this, "Destination not set.", Toast.LENGTH_SHORT).show(); break;
										} else{ 
											this.findViewById(R.id.destination).setTag(( ((EditText) this.findViewById(R.id.destination)).getText()).toString().replaceAll(" ","+")); 
										} 
								}
							 String uri = "https://maps.google.com/maps?saddr="+(CharSequence)this.findViewById(R.id.source).getTag()+ "&daddr="+(CharSequence)this.findViewById(R.id.destination).getTag()+"&dirflg=w";
							 Intent intent2 = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
							 intent2.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
							 startActivity(intent2); break;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(resultCode == 1)
			if(requestCode == 1){
				((EditText) this.findViewById(R.id.source)).setTag(data.getExtras().getString("gps"));
				((EditText) this.findViewById(R.id.source)).setText(data.getExtras().getString("name"));
			}else if(requestCode == 2){
				((EditText) this.findViewById(R.id.destination)).setTag(data.getExtras().getString("gps"));
				((EditText) this.findViewById(R.id.destination)).setText(data.getExtras().getString("name"));
			}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onCheckedChanged(CompoundButton v, boolean isChecked) {
		switch(v.getId()){
			case R.id.set_source_as_current: if(isChecked){
				((CheckBox)this.findViewById(R.id.set_destination_as_current)).setChecked(false);
				findViewById(R.id.source).setTag("");
				((EditText) findViewById(R.id.source)).setText("My Location");
			}else{
				findViewById(R.id.source).setTag(null);
				((EditText) findViewById(R.id.source)).setText("");
			}break;
			
			case R.id.set_destination_as_current: if(isChecked){
				((CheckBox)this.findViewById(R.id.set_source_as_current)).setChecked(false);
				findViewById(R.id.destination).setTag("");
				((EditText) findViewById(R.id.destination)).setText("My Location");
			}else{
				findViewById(R.id.destination).setTag(null);
				((EditText) findViewById(R.id.destination)).setText("");
			}break;
		}
	}

	@Override
	public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		if(event != null){
			Toast.makeText(this, "sda", Toast.LENGTH_SHORT).show();
		}
		return false;
	}
}
