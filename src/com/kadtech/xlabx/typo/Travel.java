package com.kadtech.xlabx.typo;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import org.apache.http.HttpEntity;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class Travel extends Activity implements OnClickListener{
	public static boolean menu_vis = false;
	private TextView content;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_travel);
		Dropdown.action_menu = (View)this.findViewById(R.id.scrollView2);
		Dropdown.menu_button = (View)this.findViewById(R.id.buttton_menu);
		Dropdown.delay = new Handler();
		content = (TextView) this.findViewById(R.id.travel_content);
		this.setResult(0);
		refresh();
	}
	private class We extends AsyncTask<String, Void, String> {

		public String result = null;
	      @Override
	      protected String doInBackground(String... params) {
	    	  try{
	    		  HttpClient httpClient = new DefaultHttpClient();
	    			HttpGet httpGet = new HttpGet("http://www.typoday.in/notifications.php?t=");
	    			try {
	    				HttpEntity httpEntity = httpClient.execute(httpGet).getEntity();
	    				if (httpEntity != null) {
	    					InputStream inputStream = httpEntity.getContent();
	    					Reader in = new InputStreamReader(inputStream);
	    					BufferedReader bufferedreader = new BufferedReader(in);
	    					StringBuilder stringBuilder = new StringBuilder();

	    					String stringReadLine = null;

	    					while ((stringReadLine = bufferedreader.readLine()) != null) {
	    						stringBuilder.append(stringReadLine + "\n");
	    					}

	    					result = stringBuilder.toString();
	    				}

	    			} catch (Exception e) {
	    				e.printStackTrace();
	    			} catch (Error e){
	    				e.printStackTrace();
	    			}
	    	  }catch(Exception w){ w.printStackTrace();}
	            return result;
	      }      
	
	      @Override
	      protected void onPostExecute(String res) {  
	    	  if(result!=null)
	    	  finished(result);
	    	  else{
	    		  failed();
	    	  }
	      }
	
	      @Override
	      protected void onPreExecute() {
	      }
	
	      @Override
	      protected void onProgressUpdate(Void... values) {
	      }
	}
	public void refresh(){
		new We().execute("");
	}
	
	public void finished(String result) {
		content.setOnClickListener(null);
		content.setText(Html.fromHtml(result));
		Linkify.addLinks(content, Linkify.ALL);
		content.setMovementMethod(LinkMovementMethod.getInstance());
		content.setPadding(0, 0, 0, 0);
	}

	public void failed() {
		content.setText("Loading failed... ");
		content.postDelayed(new Runnable(){

			@Override
			public void run() {
				Travel.this.content.setText("Click to retry now ");
				Travel.this.content.setOnClickListener(new OnClickListener(){

					@Override
					public void onClick(View v) {
						Travel.this.refresh();
						Travel.this.content.setText("Retrying... ");
					}});
			}}, 2000);
	}
	@Override
	public void onResume()
	{
		super.onResume();
		Dropdown.action_menu = (View)this.findViewById(R.id.scrollView2);
		Dropdown.menu_button = (View)this.findViewById(R.id.buttton_menu);
		Dropdown.delay = new Handler();
	}

	@Override
	public void onClick(View v) {
		switch(v.getId())
		{
			case R.id.buttton_menu: menu_vis = Dropdown.Drop(menu_vis); break;
			case R.id.button_back:
			case R.id.home_menu: this.finish(); break;
			case R.id.about_us_menu:
			case R.id.contact_menu:
			case R.id.workshops_menu:
			case R.id.results_menu:
			case R.id.lodging_menu:
			case R.id.travel_menu:
			case R.id.weather_menu:
			case R.id.speakers_menu: ActionBar.setClick(v); this.setResult(1); this.finish();
		}
	}
}
