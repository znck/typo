package com.kadtech.xlabx.typo;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.Space;
import android.text.Html;
import android.text.util.Linkify;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Lodging extends Activity implements OnClickListener {
	public static boolean menu_vis = false;
	private OnClickListener onclick = new OnClickListener(){

		@Override
		public void onClick(View v) {
			if((((ImageView)v).getContentDescription()).equals("1"))
			{
				((ImageView)v).setImageResource(R.drawable.up);
				((ImageView)v).setContentDescription("2");
				drop(v);
			}
			else{
				((ImageView)v).setContentDescription("1");
				((ImageView)v).setImageResource(R.drawable.down);
				LinearLayout target = (LinearLayout)( (v.getParent()).getParent());
				((ViewGroup) target.getParent()).removeViewAt(((ViewGroup) target.getParent()).indexOfChild(target)+1);
			}
		}
		
	};

	private OnClickListener mapClick = new OnClickListener(){

		@Override
		public void onClick(View v) {
			launchMap(v);
		}
		
	};
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_lodging);
		Dropdown.action_menu = (View)this.findViewById(R.id.scrollView2);
		Dropdown.menu_button = (View)this.findViewById(R.id.buttton_menu);
		Dropdown.delay = new Handler();
		this.setResult(0);
		load(Database.p_type);
	}
	
	protected void launchMap(View v) {
		String uri = "geo:"+(CharSequence)v.getTag()+ "";
		startActivity(new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri)));
	}

	public void drop(View v){
		
		int place_id = Integer.parseInt(""+v.getTag());
		
		LinearLayout target = (LinearLayout)( (v.getParent()).getParent());
		LinearLayout.LayoutParams eleParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		LinearLayout.LayoutParams spaceParams = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT);
		LinearLayout.LayoutParams ratingParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		LinearLayout.LayoutParams textParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		spaceParams.weight = 1f;
		eleParams.setMargins(4,0,4,0);
		ratingParams.gravity = Gravity.CENTER_VERTICAL;
		textParams.setMargins(16, 16, 16, 0);
		
		LinearLayout detail = new LinearLayout(this);
		ImageView map = new ImageView(this);
		LinearLayout data = new LinearLayout(this);
		
		LinearLayout.LayoutParams mpwpmargin = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		mpwpmargin.bottomMargin = 4;
		
		data.setOrientation(LinearLayout.VERTICAL);
		
		detail.setLayoutParams(mpwpmargin);
		detail.setOrientation(LinearLayout.VERTICAL);

		((ViewGroup) target.getParent()).addView(detail, ((ViewGroup) target.getParent()).indexOfChild(target)+1);
		
		detail.setBackgroundResource(R.drawable.whiteback);
		detail.setPadding(8, 8, 8, 24);
		
		detail.addView(map);
		detail.addView(data);
		
		mpwpmargin = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		mpwpmargin.setMargins(16,16,16,16);
		mpwpmargin.gravity = Gravity.CENTER;
		map.setLayoutParams(mpwpmargin);
		map.setBackgroundResource(R.color.grey);
		map.setPadding(8,8,8,8);
		
		map.setImageResource(R.drawable.map);
		
		data.setPadding(8, 0, 8, 0);
		
		TextView address_text = new TextView(this);
		TextView phone_text = new TextView(this);
		TextView website_text = new TextView(this);
		TextView disclaimer_text = new TextView(this);
		TextView disclaimer2_text = new TextView(this);
		data.addView(address_text);
		data.addView(phone_text);
		
		phone_text.setCompoundDrawablesWithIntrinsicBounds(R.drawable.redphonenumber,0, 0, 0);
		website_text.setCompoundDrawablesWithIntrinsicBounds(R.drawable.redwebsite,0, 0, 0);
		address_text.setCompoundDrawablesWithIntrinsicBounds(R.drawable.redaddress,0, 0, 0);
		
		Database_Handler db = new Database_Handler();;
		boolean remove = false;		
			Cursor cur = db.getHotel(place_id);
			cur.moveToFirst();
			address_text.setText("  "+cur.getString(cur.getColumnIndex(Database.p_add)).replaceAll("\n", "\n  "));
			phone_text.setText(" "+cur.getString(cur.getColumnIndex(Database.p_contact)).replaceAll("\n", "\n "));
			Linkify.addLinks(phone_text, Linkify.PHONE_NUMBERS);
			disclaimer_text.setText("*Govt. tax applicable as per standard.\n"+cur.getString(cur.getColumnIndex(Database.p_extra)));
			disclaimer2_text.setText("Terms and conditions");
			disclaimer2_text.setTextColor(this.getResources().getColor(R.color.red));
			disclaimer2_text.setPadding(0, 8, 0, 8);
			map.setTag(cur.getString(cur.getColumnIndex(Database.p_gps))+"?q=Hotel+"+cur.getString(cur.getColumnIndex(Database.p_place)).replaceAll(" ","+")+",+Guwahati,+Assam,+India&z=18");
			map.setOnClickListener(mapClick );
			if(cur.getString(cur.getColumnIndex(Database.p_type)).equalsIgnoreCase("rec")){
				phone_text.setText(" "+cur.getString(cur.getColumnIndex(Database.p_detail)));
				phone_text.setPadding(0, 16, 0, 0);
				phone_text.setCompoundDrawablesWithIntrinsicBounds(R.drawable.rupee, 0, 0, 0);
				remove =true;
			}else{
				String st = cur.getString(cur.getColumnIndex(Database.p_website)).substring(7);
				website_text.setText(Html.fromHtml("<a href=\""+cur.getString(cur.getColumnIndex(Database.p_website))+"\">"+st.substring(0,st.length() -1)+"</a>"));
			}
			cur.close();
			db.close();
		
		if(!remove){
			data.addView(website_text);
			Linkify.addLinks(website_text, Linkify.WEB_URLS);
		}
		data.addView(disclaimer_text);
		if(remove){
		data.addView(disclaimer2_text);
		
		disclaimer2_text.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				launchDialog();
			}});
		}
		
		address_text.setTextSize(18);
		phone_text.setTextSize(18);
		website_text.setTextSize(18);
		
		address_text.setTextColor(this.getResources().getColor(R.color.Black));
		
		phone_text.setLinkTextColor(this.getResources().getColor(R.color.Black));
		website_text.setLinkTextColor(this.getResources().getColor(R.color.Black));
		disclaimer_text.setLinkTextColor(this.getResources().getColor(R.color.Black));
		disclaimer2_text.setLinkTextColor(this.getResources().getColor(R.color.Black));
		
		address_text.setPadding(16, 0, 0, 0);
		phone_text.setPadding(16, 0, 0, 0);
		website_text.setPadding(16, 0, 0, 0);
		disclaimer_text.setPadding(48, 0, 0, 0);
		disclaimer2_text.setPadding(48, 0, 0, 0);
		
		LinearLayout.LayoutParams rightAlign = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		rightAlign.gravity = Gravity.RIGHT;
		
	}
	
	public void launchDialog(){
		this.startActivity(new Intent(this, Disclaimer.class));
	}

	@Override
	public void onResume()
	{
		super.onResume();
		Dropdown.action_menu = (View)this.findViewById(R.id.scrollView2);
		Dropdown.menu_button = (View)this.findViewById(R.id.buttton_menu);
		Dropdown.delay = new Handler();
	}
	
	private void load(String orderby)
	{
		try{
			Database_Handler db = new Database_Handler();
			Cursor cur = db.getHotels(orderby);
			LinearLayout cont = (LinearLayout)this.findViewById(R.id.list_hotels);
			LinearLayout.LayoutParams eleParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
			LinearLayout.LayoutParams subParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
			LinearLayout.LayoutParams spaceParams = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT);
			LinearLayout.LayoutParams ratingParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
			LinearLayout.LayoutParams textParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
			spaceParams.weight = 1f;
			ratingParams.gravity = Gravity.CENTER_VERTICAL;
			textParams.setMargins(16, 16, 16, 0);
			cur.moveToFirst();
			for(int i = 0; i < cur.getCount(); ++i)
			{
				LinearLayout ele = new LinearLayout(this);
				ele.setLayoutParams(eleParams);
				cont.addView(ele);
				
				ele.setOrientation(LinearLayout.HORIZONTAL);
				ele.setBackgroundResource(R.drawable.redback);
				
				TextView tv = new TextView(this); 
				tv.setLayoutParams(textParams);
					ele.addView(tv);
				tv.setTextColor(getResources().getColor(R.color.White));
				tv.setText(cur.getString(cur.getColumnIndex(Database.p_place)));
				tv.setTextSize(20);
				tv.setPadding(16, 0, 16, 0);
				tv.setTypeface(null, Typeface.BOLD);
				if(cur.getString(cur.getColumnIndex(Database.p_type)).equalsIgnoreCase("rec"))
				{
					tv.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.recommended, 0);
				}
				LinearLayout sub = new LinearLayout(this);
				sub.setLayoutParams(subParams);
				
				ele.addView(sub);
				
				sub.setPadding(32, 0, 16, 8);
				
				ele.setOrientation(LinearLayout.VERTICAL);
				
				ImageView rating = new ImageView(this);
					sub.addView(rating);
				
				rating.setLayoutParams(ratingParams);
				
				Space sp = new Space(this);
					sp.setLayoutParams(spaceParams);
					sub.addView(sp);
				
				ImageButton down = new ImageButton(this);
					sub.addView(down);
				
				down.setImageResource(R.drawable.down);
				down.setBackgroundResource(R.color.transparent);
				down.setContentDescription("1");
				down.setOnClickListener(onclick);
				down.setTag(cur.getString(cur.getColumnIndex(Database.id)));
				rating.setImageResource(this.getResources().getIdentifier("s"+cur.getString(cur.getColumnIndex(Database.p_rate)), "drawable", this.getPackageName()));
				
				cur.moveToNext();
			}
			cur.close();
			db.close();
		}catch(Exception i){}
	}
	@Override
	public void onClick(View v) {
		switch(v.getId())
		{
			case R.id.buttton_menu: menu_vis = Dropdown.Drop(menu_vis); break;
			case R.id.button_back:
			case R.id.home_menu: this.finish(); break;
			case R.id.about_us_menu:
			case R.id.contact_menu:
			case R.id.workshops_menu:
			case R.id.results_menu:
			case R.id.lodging_menu:
			case R.id.travel_menu:
			case R.id.weather_menu:
			case R.id.speakers_menu: ActionBar.setClick(v); this.setResult(1); this.finish();
		}
	}

}
