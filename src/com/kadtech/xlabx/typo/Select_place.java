package com.kadtech.xlabx.typo;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannedString;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class Select_place extends Activity implements OnClickListener{
	private List<View> offsets = new ArrayList<View>();
	private List<String> keys = new ArrayList<String>();

	private OnClickListener l = new OnClickListener(){

		@Override
		public void onClick(View v) {
			setResultandExit(v);
		}};
	private OnClickListener lm = new OnClickListener(){

		@Override
		public void onClick(View v) {
			setResultandExit(((ViewGroup) v.getParent()).getChildAt(((ViewGroup)v.getParent()).indexOfChild(v) - 1));
		}};
	private OnClickListener svc = new OnClickListener(){
		@Override
		public void onClick(View v) {
			scrollto((Integer) v.getTag());
		}
	};
	private ScrollView sv;
	private void setResultandExit(View v){
		Intent i =new Intent();
		i.putExtra("gps", (String) v.getTag());
		i.putExtra("name", ((SpannedString) ((TextView) v).getText()).toString());
		this.setResult(1,i);
		this.finish();
	}
	protected void scrollto(int tag) {
		try{
		sv.smoothScrollTo(0,  (offsets.get(tag).getTop()));		}catch(Exception e){};
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_select_place);
		((TextView) this.findViewById(R.id.actionbar_title)).setText(getIntent().getStringExtra("title"));
		sv = (ScrollView) this.findViewById(R.id.scrollView1);
		sideBar();
		content();
	}
	
	private void sideBar(){
		Database_Handler db = new Database_Handler();
		Cursor cur = db.getPlaceLetters();
		LinearLayout lside = (LinearLayout) this.findViewById(R.id.sidebar);
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,0);
		lp.weight = 1f;
		lp.gravity = Gravity.CENTER;
		TextView side;
		cur.moveToFirst();int i =0;
		while(i < cur.getCount()){
			side = new TextView(this);
			side.setLayoutParams(lp);
			lside.addView(side);
			side.setText(cur.getString(cur.getColumnIndex("let")));
			keys.add(cur.getString(cur.getColumnIndex("let")));
			side.setPadding(8, 0, 8, 0);
			side.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
			side.setOnClickListener(svc );
			side.setTag(i);
			cur.moveToNext();
			i++;
		}
		cur.close();
		db.close();
	}
	private void content(){
		Database_Handler db = new Database_Handler();
		Cursor cur = db.getPlace();
		LinearLayout lside = (LinearLayout) this.findViewById(R.id.selectable);
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,0);
		lp.weight = 1f;
		TextView side,foot;
		cur.moveToFirst();int i =0,j=0;
		boolean set = false;
		ImageView iv;
		while(i++ < cur.getCount()){
			side = new TextView(this);
			foot = new TextView(this);
			side.setLayoutParams(lp);
			foot.setLayoutParams(lp);
			iv = new ImageView(this);
			iv.setImageResource(R.drawable.redline);
			lside.addView(side);
			lside.addView(foot);
			lside.addView(iv);
			side.setText(Html.fromHtml("<b>"+cur.getString(cur.getColumnIndex(Database.g_name))+"</br>"));
			side.setTextColor(this.getResources().getColor(R.color.Black));
			side.setTextSize(20);
			side.setTag(cur.getString(cur.getColumnIndex(Database.g_info)));
			foot.setTag(cur.getString(cur.getColumnIndex(Database.g_info)));
			foot.setText(cur.getString(cur.getColumnIndex(Database.g_alt)));
			foot.setTextColor(this.getResources().getColor(R.color.Black));
			foot.setPadding(32, 0, 60, 32);
			side.setPadding(32, 16, 60, 0);
			side.setOnClickListener(l );
			foot.setOnClickListener(lm);
			if(!cur.getString(cur.getColumnIndex(Database.g_name)).startsWith(keys.get(j))){
				j++;
				set=false;
			}
			if(!set){
				set=true;
				offsets.add(side);
			}
			cur.moveToNext();
		}
		cur.close();
		db.close();
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
			case R.id.button_back: this.setResult(0); this.finish(); break; 
		}
	}
	
}
