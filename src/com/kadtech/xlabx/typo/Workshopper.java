package com.kadtech.xlabx.typo;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

public class Workshopper extends Activity implements OnClickListener {
	public static boolean menu_vis = false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_workshopper);
		Dropdown.action_menu = (View)this.findViewById(R.id.scrollView2);
		Dropdown.menu_button = (View)this.findViewById(R.id.buttton_menu);
		Dropdown.delay = new Handler();
		load(this.getIntent().getIntExtra("id", 1));
	}

	private void load(int intExtra) {
		Database_Handler db = new Database_Handler();
		Cursor cur = db.getWorkshops(intExtra);
		cur.moveToFirst();
			
			if(cur.getString(cur.getColumnIndex(Database.x_image)).split(";").length > 1){
				String images[] = cur.getString(cur.getColumnIndex(Database.x_image)).split(";");
				String names[] = cur.getString(cur.getColumnIndex(Database.x_name)).split(";");
				String posts[] = cur.getString(cur.getColumnIndex(Database.x_post)).split(";");
				((ImageView) this.findViewById(R.id.workshopper_image)).setImageResource(this.getId(images[0]+"2", "drawable"));
				((TextView) this.findViewById(R.id.workshopper_name)).setText(names[0]);
				((TextView) this.findViewById(R.id.workshopper_post)).setText(posts[0]);
				((TextView) this.findViewById(R.id.workshopper_name)).setPadding(0, 8, 0, 0);
				((TextView) this.findViewById(R.id.workshopper_post)).setPadding(0, 8, 0, 0);
				((TextView) this.findViewById(R.id.workshopper_detail)).setText(Html.fromHtml("<b>"+cur.getString(cur.getColumnIndex(Database.x_title))+"</b><br /><br />"+cur.getString(cur.getColumnIndex(Database.x_text))));
				((TextView) this.findViewById(R.id.workshopper_website)).setText(Html.fromHtml("+ <a href=\""+cur.getString(cur.getColumnIndex(Database.x_web))+"\">read more</a>"));
				Linkify.addLinks(((TextView) this.findViewById(R.id.workshopper_website)),Linkify.WEB_URLS);
				((TextView) this.findViewById(R.id.workshopper_website)).setMovementMethod(LinkMovementMethod.getInstance());
				
				int index = 3;
				ViewGroup target = (ViewGroup) this.findViewById(R.id.workshopper_post).getParent();
				TextView name = new TextView(this), post = new TextView(this);
				ImageView image = new ImageView(this);
				
				name.setText(Html.fromHtml("<b>"+names[1]+"</b>"));
				post.setText(posts[1]);
				image.setImageResource(this.getId(images[1]+"2", "drawable"));
				
				LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT);
				LinearLayout.LayoutParams tp = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT);
				lp.setMargins(40, 20, 40, 0);
				
				image.setLayoutParams(lp);
				name.setLayoutParams(tp);
				post.setLayoutParams(tp);
				
				name.setGravity(Gravity.CENTER);
				post.setGravity(Gravity.CENTER);
				
				name.setPadding(0, 8, 0, 0);
				post.setPadding(0, 8, 0, 0);
				
				name.setTextSize(18);
				post.setTextSize(12);
				
				name.setTextColor(this.getResources().getColor(R.color.Black));
				post.setTextColor(this.getResources().getColor(R.color.Black));
				
				target.addView(image,index++);
				target.addView(name, index++);
				target.addView(post, index);
				
			}else{
				((ImageView) this.findViewById(R.id.workshopper_image)).setImageResource(this.getId(cur.getString(cur.getColumnIndex(Database.x_image))+"2", "drawable"));
				((TextView) this.findViewById(R.id.workshopper_name)).setText(cur.getString(cur.getColumnIndex(Database.x_name)));
				((TextView) this.findViewById(R.id.workshopper_post)).setText(cur.getString(cur.getColumnIndex(Database.x_post)));
				((TextView) this.findViewById(R.id.workshopper_name)).setPadding(0, 8, 0, 0);
				((TextView) this.findViewById(R.id.workshopper_post)).setPadding(0, 8, 0, 0);
				((TextView) this.findViewById(R.id.workshopper_detail)).setText(Html.fromHtml("<b>"+cur.getString(cur.getColumnIndex(Database.x_title))+"</b><br /><br />"+cur.getString(cur.getColumnIndex(Database.x_text))));
				((TextView) this.findViewById(R.id.workshopper_website)).setText(Html.fromHtml("+ <a href=\""+cur.getString(cur.getColumnIndex(Database.x_web))+"\">read more</a>"));
				Linkify.addLinks(((TextView) this.findViewById(R.id.workshopper_website)),Linkify.WEB_URLS);
				((TextView) this.findViewById(R.id.workshopper_website)).setMovementMethod(LinkMovementMethod.getInstance());
			}
		
		cur.close();
		db.close();
	}

	public int getId(String name, String defType){
		return this.getResources().getIdentifier(name, defType, this.getPackageName());
	}
	
	@Override
	public void onResume()
	{
		super.onResume();
		Dropdown.action_menu = (View)this.findViewById(R.id.scrollView2);
		Dropdown.menu_button = (View)this.findViewById(R.id.buttton_menu);
		Dropdown.delay = new Handler();
	}
	@Override
	public void onClick(View v) {
		switch(v.getId()){
				case R.id.buttton_menu: menu_vis = Dropdown.Drop(menu_vis); break;
				case R.id.button_back: this.finish(); break;
				case R.id.home_menu:
				case R.id.about_us_menu:
				case R.id.contact_menu:
				case R.id.workshops_menu:
				case R.id.results_menu:
				case R.id.lodging_menu:
				case R.id.travel_menu:
				case R.id.weather_menu:
				case R.id.speakers_menu: ActionBar.setClick(v); this.setResult(1); this.finish(); break;
			}
		
	}
}
