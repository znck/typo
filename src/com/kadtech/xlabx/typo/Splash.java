package com.kadtech.xlabx.typo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class Splash extends Activity {
	public static Handler delay;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		delay = new Handler();
		delay.postDelayed(new Runnable(){

			@Override
			public void run() {
				start();				
			}}, 3000);
	}

	protected void start() {
		this.startActivityForResult(new Intent(this, Main.class), 0);
	}
	
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
	
	

	@Override
	protected void onActivityResult(int request, int result, Intent data)
	{
		this.finish();
	}

	@Override
	protected void onResume() {
		super.onResume();
	}
}
