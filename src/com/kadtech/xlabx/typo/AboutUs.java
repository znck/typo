package com.kadtech.xlabx.typo;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class AboutUs extends Activity implements OnClickListener{
	public static boolean menu_vis = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about_us);
		Dropdown.action_menu = (View)this.findViewById(R.id.scrollView2);
		Dropdown.menu_button = (View)this.findViewById(R.id.buttton_menu);
		Dropdown.delay = new Handler();
	}
	
	@Override
	public void onResume()
	{
		super.onResume();
		Dropdown.action_menu = (View)this.findViewById(R.id.scrollView2);
		Dropdown.menu_button = (View)this.findViewById(R.id.buttton_menu);
		Dropdown.delay = new Handler();
	}
	
	public void showOnClick(View v){
		LinearLayout target = (LinearLayout)v.getParent().getParent();
		LinearLayout sub = (LinearLayout) target.getChildAt(1);
		if(sub.getVisibility() == View.GONE){
			sub.setVisibility(View.VISIBLE);
			((ImageView) v).setImageResource(R.drawable.up);
			((LinearLayout)v.getParent()).setBackgroundResource(R.drawable.noshadowred);
		}
		else{
			sub.setVisibility(View.GONE);
			((ImageView) v).setImageResource(R.drawable.down);
			((LinearLayout)v.getParent()).setBackgroundResource(R.drawable.redback);
		}
	}

	@Override
	public void onClick(View v) {
		switch(v.getId())
		{
			case R.id.buttton_menu: menu_vis = Dropdown.Drop(menu_vis); break;
			case R.id.button_back:
			case R.id.home_menu: this.finish(); break;
			case R.id.about_us_menu:
			case R.id.contact_menu:
			case R.id.workshops_menu:
			case R.id.results_menu:
			case R.id.lodging_menu:
			case R.id.travel_menu:
			case R.id.weather_menu:
			case R.id.speakers_menu: ActionBar.setClick(v); this.setResult(1); this.finish();
		}
	}

}
