package com.kadtech.xlabx.typo;

import notificationLoader.NotifictionQuery;
import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Notifications extends Activity implements OnClickListener {
	private static int getterId = 0;
	private static Notifications self; 
	private static LinearLayout parent;
	private static TextView text, time, loading;
	public static Context context;
	private static boolean inprog = false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_notifications);
		context = this.getApplicationContext();
		parent = (LinearLayout) this.findViewById(R.id.list_notifications);
		loading = (TextView) this.findViewById(R.id.loadingnotif);
		self = this;
		inflateOld();
		Database_Handler db = new Database_Handler();
		Cursor cur = db.dr.rawQuery("SELECT COUNT(*) FROM "+Database.updates, null);
		cur.moveToFirst();
		boolean call = false;
		if(cur.getInt(0) < Main.countNot || Main.countNot == -1){
			call = true;
			Main.countNot = cur.getInt(0);
		}
		cur.close();
		db.close();
		if(call)
			tryNew();
	}	

	private static void tryNew() {
		inprog = true;
		loading.setVisibility(View.VISIBLE);
		new NotifictionQuery(getterId).execute("");
	}

	private void inflateOld() {
		Database_Handler db = new Database_Handler();
		try{
			Cursor cur = db.dr.rawQuery("SELECT * FROM "+Database.updates+" ORDER BY "+Database.id+" DESC;", null);
			cur.moveToFirst();
			getterId =cur.getCount() + 1;
			if(cur.getCount() > 0){
				parent.removeAllViews();
				while(!cur.isAfterLast()){
					Log.i("sadfas","adding new");
					addNew(cur.getString(cur.getColumnIndex(Database.u_text)), -1, cur.getInt(cur.getColumnIndex(Database.u_seen)));
					cur.moveToNext();
				}
			}
			cur.close();
			db.dw.execSQL("UPDATE "+Database.updates+" SET "+Database.u_seen+"='2'");
		}catch(Exception e){
			e.printStackTrace();
		}
		db.close();
	}

	public static void uiUpdate(String result) {
		inprog = false;
		loading.setVisibility(View.GONE);
		if(result != null){
		Database_Handler db = new Database_Handler();
			try{
				String updates[] = result.split("<-end->");
				Main.countNot = updates.length;
					try{
						db.dw.execSQL("DELETE FROM "+Database.updates);
					for(int i = 0; i < updates.length; i++){
						Log.i("sdfssfasdfadfasfasdf", updates[i]);
						db.dw.execSQL("INSERT INTO `"+Database.updates+"` ( `id`, `"+Database.u_text+"`, `"+Database.u_seen+"` ) VALUES( null, \""+(updates[i]).replaceAll("\"","&kad;")+"\", '1');");
					}}catch(Exception e){e.printStackTrace();}
			}catch(Exception e){
				e.printStackTrace();
			}
			db.close();
			Notifications.self.inflateOld();
		} else {
			Toast.makeText(Notifications.context, "Failed..!!! No Network Connection", Toast.LENGTH_SHORT).show();
		}
	}
	
	public void addNew(String value, int index,int seen){
		text = new TextView(this);
		time = new TextView(this);
		value = value.replaceAll("&kad;", "\"");
		if(value.indexOf("###") > -1){
			text.setText(Html.fromHtml(value.split("###")[0]));
			time.setText(Html.fromHtml(value.split("###")[1]));
			time.setTextColor(this.getResources().getColor(R.color.red));
			text.setTextColor(this.getResources().getColor(R.color.Black));
			time.setTextSize(11);
			text.setTextSize(14);
			text.setPadding(8, 8, 8, 0);
			time.setPadding(8, 0, 8, 16);
			text.setBackgroundResource(R.color.White);
			time.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, R.drawable.redline);
			Linkify.addLinks(text, Linkify.ALL);
			text.setMovementMethod(LinkMovementMethod.getInstance());
			if(index == -1){
				parent.addView(text);
				parent.addView(time);
			}else{
				parent.addView(time, index);
				parent.addView(text, index);
			}
		}
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
			case R.id.button_back: this.finish(); break;
			case R.id.buttton_refresh: if(!inprog){loading.setVisibility(View.VISIBLE);tryNew();} break;
		}
	}

}
