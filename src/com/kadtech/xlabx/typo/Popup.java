package com.kadtech.xlabx.typo;

import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

public class Popup extends Activity implements OnClickListener{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_popup);
		Bundle b = this.getIntent().getExtras();
		((TextView) this.findViewById(R.id.title)).setText(b.getString("title"));
		String cont[] = b.getString("content").split("###");
		ViewGroup c = (ViewGroup) this.findViewById(R.id.content);
		for(int i = 0; i < cont.length; ++i){
			TextView tv = new TextView(this);
			tv.setText(Html.fromHtml(cont[i]));
			tv.setTextColor(this.getResources().getColor(R.color.Black));
			Linkify.addLinks(tv, Linkify.ALL);
			tv.setMovementMethod(LinkMovementMethod.getInstance());
			c.addView(tv);
		}
	}

	@Override
	public void onClick(View v) {
		this.finish();
	}
	

}
