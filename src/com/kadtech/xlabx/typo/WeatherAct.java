package com.kadtech.xlabx.typo;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import android.app.Activity;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.kadtech.xlabx.typo.weatherApi.WeatherInfo;
import com.kadtech.xlabx.typo.weatherApi.YahooWeatherUtils;

public class WeatherAct extends Activity implements OnClickListener {
	public static boolean menu_vis = false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_weather);
		Dropdown.action_menu = (View)this.findViewById(R.id.scrollView2);
		Dropdown.menu_button = (View)this.findViewById(R.id.buttton_menu);
		Dropdown.delay = new Handler();
		this.setResult(0);
		setList();
		load();
		setWeather();
	}
	private void load() {
		Database_Handler db = new Database_Handler();
		Cursor cur = db.getWeather();
		if(cur.getCount() <1){
			cur.close();
			db.close();
		}else{
		cur.moveToFirst();
			((TextView) this.findViewById(R.id.today_day)).setText(cur.getString(cur.getColumnIndex(Database.w_day)));
			((TextView) this.findViewById(R.id.today_date)).setText(cur.getString(cur.getColumnIndex(Database.w_date)));
			((TextView) this.findViewById(R.id.today_temp)).setText(cur.getString(cur.getColumnIndex(Database.w_temp)));
			((ImageView) this.findViewById(R.id.today_image)).setImageResource(cur.getInt(cur.getColumnIndex(Database.w_image)));
		
		if(!cur.isLast())
		cur.moveToNext();
		
			((TextView) this.findViewById(R.id.day1_day)).setText(cur.getString(cur.getColumnIndex(Database.w_day)));
			((TextView) this.findViewById(R.id.day1_date)).setText(cur.getString(cur.getColumnIndex(Database.w_date)));
			((TextView) this.findViewById(R.id.day1_temp)).setText(cur.getString(cur.getColumnIndex(Database.w_temp)));
			((ImageView) this.findViewById(R.id.day1_image)).setImageResource(cur.getInt(cur.getColumnIndex(Database.w_image)));
			
		if(!cur.isLast())
		cur.moveToNext();
			
			((TextView) this.findViewById(R.id.day2_day)).setText(cur.getString(cur.getColumnIndex(Database.w_day)));
			((TextView) this.findViewById(R.id.day2_date)).setText(cur.getString(cur.getColumnIndex(Database.w_date)));
			((TextView) this.findViewById(R.id.day2_temp)).setText(cur.getString(cur.getColumnIndex(Database.w_temp)));
			((ImageView) this.findViewById(R.id.day2_image)).setImageResource(cur.getInt(cur.getColumnIndex(Database.w_image)));
			
		cur.close();
		db.close();
		findViewById(R.id.weather_childs).setVisibility(View.VISIBLE);
		}
	}
	private void setWeather() {
		refresh();
	}
	
	private class We extends AsyncTask<String, Void, String> {

		public WeatherInfo weather = null;
	      @Override
	      protected String doInBackground(String... params) {
	    	  try{
	  		YahooWeatherUtils weatherUtils = YahooWeatherUtils.getInstance();
			weather = weatherUtils.queryYahooWeather();
	    	  }catch(Exception w){ w.printStackTrace();}
	            return null;
	      }      
	
	      @Override
	      protected void onPostExecute(String result) {  
	    	  if(weather!=null)
	    	  finished(weather);
	    	  else{
	    		  failed();
	    	  }
	      }
	
	      @Override
	      protected void onPreExecute() {
	      }
	
	      @Override
	      protected void onProgressUpdate(Void... values) {
	      }
	}
	public void refresh(){
		new We().execute("");
	}
	
	public void failed() {
		((TextView) this.findViewById(R.id.loading)).setText("Loading failed... ");
		this.findViewById(R.id.loading).postDelayed(new Runnable(){

			@Override
			public void run() {
				((TextView) WeatherAct.this.findViewById(R.id.loading)).setText("Click to retry now ");
				WeatherAct.this.findViewById(R.id.loading).setOnClickListener(new OnClickListener(){

					@Override
					public void onClick(View v) {
						WeatherAct.this.refresh();
						((TextView) WeatherAct.this.findViewById(R.id.loading)).setText("Retrying... ");
					}});
			}}, 2000);
	}
	public void finished(WeatherInfo w)
	{
		long image;
		Calendar c = Calendar.getInstance();
		boolean night = (c.get(Calendar.HOUR_OF_DAY) > 19 || c.get(Calendar.HOUR_OF_DAY) < 5)? true: false;
		switch(w.getCurrentCode())
		{
			case 31: //clear
			case 32:
			case 33:
			case 34:
			case 36: 
					if(night){
						((ImageView) this.findViewById(R.id.today_image)).setImageResource(R.drawable.clear_night);
						image = R.drawable.clear_night;
					}
					else{
						((ImageView) this.findViewById(R.id.today_image)).setImageResource(R.drawable.sunny_clear);
						image = R.drawable.sunny_clear;
					}
					break;
			case 21:
			case 22:
			case 23:
			case 24:
			case 25:
			case 26:
			case 27:
			case 28:
			case 29:
			case 30:
			case 44: 
				if(night){
					((ImageView) this.findViewById(R.id.today_image)).setImageResource(R.drawable.cloudy_night);
					image = R.drawable.cloudy_night;
				}
				else{
					((ImageView) this.findViewById(R.id.today_image)).setImageResource(R.drawable.cloudy);
					image = R.drawable.cloudy;
				}
					break;
			default: 
				if(night){
					((ImageView) this.findViewById(R.id.today_image)).setImageResource(R.drawable.cloudy_night);
					image = R.drawable.cloudy_night;
				}
				else{
					((ImageView) this.findViewById(R.id.today_image)).setImageResource(R.drawable.thunderstorm);
					image = R.drawable.thunderstorm;
				}
		}
		SimpleDateFormat sdf = new SimpleDateFormat("d MMM", Locale.US);
		sdf.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
		((TextView) this.findViewById(R.id.today_day)).setText("Today");
		((TextView) this.findViewById(R.id.today_date)).setText(sdf.format(new Date()));
		((TextView) this.findViewById(R.id.today_temp)).setText(w.getCurrentTempC()+(this.getResources().getString(R.string.deg))+" ");
		Database_Handler db = new Database_Handler();
		db.clearWeather();
		sdf = new SimpleDateFormat("EEEEEE,d MMM", Locale.US);
		sdf.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
		db.dw.execSQL("INSERT INTO `weather` ( id, day, date, image, temp) VALUES( null, '"+(sdf.format(new Date(System.currentTimeMillis()))).replace(",", "', '")+"', '"+image+"', '"+w.getCurrentTempC()+(this.getResources().getString(R.string.deg))+"');");
		
		switch(w.getForecast1Code())
		{
		case 31: //clear
		case 32:
		case 33:
		case 34:
		case 36: 
			{
					((ImageView) this.findViewById(R.id.day1_image)).setImageResource(R.drawable.sunny_clear);
					image = R.drawable.sunny_clear;
				}
				break;
		case 21:
		case 22:
		case 23:
		case 24:
		case 25:
		case 26:
		case 27:
		case 28:
		case 29:
		case 30:
		case 44: 
			{
				((ImageView) this.findViewById(R.id.day1_image)).setImageResource(R.drawable.cloudy);
				image = R.drawable.cloudy;
			}
				break;
		default: 
			{
				((ImageView) this.findViewById(R.id.day1_image)).setImageResource(R.drawable.thunderstorm);
				image = R.drawable.thunderstorm;
			}

		}
		sdf = new SimpleDateFormat("EEEEEE", Locale.US);
		sdf.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
		((TextView) this.findViewById(R.id.day1_day)).setText(sdf.format(new Date(System.currentTimeMillis()+(24*60*60*1000)))+"");
		sdf = new SimpleDateFormat("d MMM", Locale.US);
		sdf.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
		((TextView) this.findViewById(R.id.day1_date)).setText(sdf.format(new Date(System.currentTimeMillis()+(24*60*60*1000))));
		//((TextView) this.findViewById(R.id.day1_temp)).setText(" ");
		((TextView) this.findViewById(R.id.day1_temp)).setText(w.getForecast1TempHighC()+(this.getResources().getString(R.string.deg))+"/"+w.getForecast1TempLowC()+(this.getResources().getString(R.string.deg)));
		
		sdf = new SimpleDateFormat("EEEEEE,d MMM", Locale.US);
		sdf.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
		db.dw.execSQL("INSERT INTO `weather` ( id, day, date, image, temp) VALUES( null, '"+(sdf.format(new Date(System.currentTimeMillis()+(24*60*60*1000)))).replace(",", "', '")+"', '"+image+"', '"+w.getForecast1TempHighC()+(this.getResources().getString(R.string.deg))+"/"+w.getForecast1TempLowC()+(this.getResources().getString(R.string.deg))+"');");
		
		switch(w.getForecast2Code())
		{
		case 31: //clear
		case 32:
		case 33:
		case 34:
		case 36: 
				{
					((ImageView) this.findViewById(R.id.day2_image)).setImageResource(R.drawable.sunny_clear);
					image = R.drawable.sunny_clear;
				}
				break;
		case 21:
		case 22:
		case 23:
		case 24:
		case 25:
		case 26:
		case 27:
		case 28:
		case 29:
		case 30:
		case 44: 
		{
				((ImageView) this.findViewById(R.id.day2_image)).setImageResource(R.drawable.cloudy);
				image = R.drawable.cloudy;
			}
				break;
		default: 
			{
				((ImageView) this.findViewById(R.id.day2_image)).setImageResource(R.drawable.thunderstorm);
				image = R.drawable.thunderstorm;
			}
		}
		sdf = new SimpleDateFormat("EEEEEE", Locale.US);
		sdf.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
		((TextView) this.findViewById(R.id.day2_day)).setText(sdf.format(new Date(System.currentTimeMillis()+(2*24*60*60*1000)))+"");
		sdf = new SimpleDateFormat("d MMM", Locale.US);
		sdf.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
		((TextView) this.findViewById(R.id.day2_date)).setText(sdf.format(new Date(System.currentTimeMillis()+(2*24*60*60*1000))));
		//((TextView) this.findViewById(R.id.day1_temp)).setText(" ");
		((TextView) this.findViewById(R.id.day2_temp)).setText(w.getForecast2TempHighC()+(this.getResources().getString(R.string.deg))+"/"+w.getForecast2TempLowC()+(this.getResources().getString(R.string.deg)));
		
		sdf = new SimpleDateFormat("EEEEEE,d MMM", Locale.US);
		sdf.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
		db.dw.execSQL("INSERT INTO `weather` ( id, day, date, image, temp) VALUES( null, '"+(sdf.format(new Date(System.currentTimeMillis()+(24*60*60*1000*2)))).replace(",", "', '")+"', '"+image+"', '"+w.getForecast1TempHighC()+(this.getResources().getString(R.string.deg))+"/"+w.getForecast2TempLowC()+(this.getResources().getString(R.string.deg))+"');");
		db.close();
		findViewById(R.id.weather_childs).setVisibility(View.VISIBLE);
		this.findViewById(R.id.loading).setVisibility(View.GONE);
	}
	@Override
	public void onResume()
	{
		super.onResume();
		Dropdown.action_menu = (View)this.findViewById(R.id.scrollView2);
		Dropdown.menu_button = (View)this.findViewById(R.id.buttton_menu);
		Dropdown.delay = new Handler();
	}

	@Override
	public void onClick(View v) {
		switch(v.getId())
		{
			case R.id.buttton_menu: menu_vis = Dropdown.Drop(menu_vis); break;
			case R.id.button_back:
			case R.id.home_menu: this.finish(); break;
			case R.id.about_us_menu:
			case R.id.contact_menu:
			case R.id.workshops_menu:
			case R.id.results_menu:
			case R.id.lodging_menu:
			case R.id.travel_menu:
			case R.id.weather_menu:
			case R.id.speakers_menu: ActionBar.setClick(v); this.setResult(1); this.finish();
		}
	}
	
	public void hsClick(View v){
		LinearLayout target = (LinearLayout)v.getParent().getParent();
		View sub = target.getChildAt(1);
		if(sub.getVisibility() == View.GONE){
			sub.setVisibility(View.VISIBLE);
			((TextView) v).setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.up,0);
		}
		else{
			sub.setVisibility(View.GONE);
			((TextView) v).setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.down,0);
		}
	}
	private void setList()
	{
		ScrollView temp = (ScrollView)this.findViewById(R.id.scrollView1);
		LinearLayout ll = (LinearLayout) temp.getChildAt(0);
		LinearLayout ele;
		for(int i = 0; i < ll.getChildCount(); ++i){
			ele = (LinearLayout) ((LinearLayout) ll.getChildAt(i)).getChildAt(0);
			ele.getChildAt(0).setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					hsClick(v);
				}});
		}
	}
}
