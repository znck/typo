package com.kadtech.xlabx.typo;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class Results extends Activity implements OnClickListener{
	public static boolean menu_vis = false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_results);
		Dropdown.action_menu = (View)this.findViewById(R.id.scrollView2);
		Dropdown.menu_button = (View)this.findViewById(R.id.buttton_menu);
		Dropdown.delay = new Handler();
		this.setResult(0);
		setList();
	}
	
	private void setList()
	{
		ScrollView temp = (ScrollView)this.findViewById(R.id.scrollView1);
		LinearLayout ll = (LinearLayout) temp.getChildAt(0);
		LinearLayout ele;
		for(int i = 0; i < ll.getChildCount(); ++i){
			ele = (LinearLayout) ((LinearLayout) ll.getChildAt(i)).getChildAt(0);
			ele.getChildAt(1).setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					hsClick(v);
				}});
		}
		LinearLayout posters = (LinearLayout) this.findViewById(R.id.posters), poster;
		TextView name,add;
		String winners[] = this.getResources().getString(R.string.poster_results).split("###");
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		LinearLayout.LayoutParams lp2 = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		lp2.bottomMargin = 2;
		lp.gravity = Gravity.CENTER_VERTICAL;
		int count = 0;
		while(count < winners.length){
			poster = new LinearLayout(this);
			poster.setPadding(32, 0, 0, 0);
			poster.setLayoutParams(lp2)
;			poster.setBackgroundResource(R.drawable.redback);
			name = new TextView(this);
			name.setText(Html.fromHtml("<b>"+winners[count++].split(",")[0]+", </b>"));
			name.setTextSize(18);
			name.setGravity(Gravity.LEFT|Gravity.CENTER_VERTICAL);
			name.setTextColor(this.getResources().getColor(R.color.White));
			name.setLayoutParams(lp);
			add = new TextView(this);
			add.setLayoutParams(lp);
			add.setLayoutParams(lp);
			add.setText(Html.fromHtml("<b>"+winners[count - 1].split(",")[1]+"</b>"));
			add.setTextSize(14);
			add.setGravity(Gravity.LEFT|Gravity.CENTER_VERTICAL);
			add.setTextColor(this.getResources().getColor(R.color.Black));
			poster.addView(name);
			poster.addView(add);
			
			posters.addView(poster);
		}
	}
	@Override
	public void onResume()
	{
		super.onResume();
		Dropdown.action_menu = (View)this.findViewById(R.id.scrollView2);
		Dropdown.menu_button = (View)this.findViewById(R.id.buttton_menu);
		Dropdown.delay = new Handler();
	}
	
	public void hsClick(View v){
		LinearLayout target = (LinearLayout)v.getParent().getParent();
		View sub = target.getChildAt(1);
		if(sub.getVisibility() == View.GONE){
			sub.setVisibility(View.VISIBLE);
			((TextView) v).setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.up,0);
			((LinearLayout)v.getParent()).setBackgroundResource(R.drawable.noshadowred);
		}
		else{
			sub.setVisibility(View.GONE);
			((TextView) v).setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.down,0);
			((LinearLayout)v.getParent()).setBackgroundResource(R.drawable.redback);
		}
	}

	@Override
	public void onClick(View v) {
		switch(v.getId())
		{
			case R.id.buttton_menu: menu_vis = Dropdown.Drop(menu_vis); break;
			case R.id.button_back:
			case R.id.home_menu: this.finish(); break;
			case R.id.button2: v.setBackgroundResource(R.drawable.whitebar); ((Button)v).setTextColor(this.getResources().getColor(R.color.Black)); ((Button)this.findViewById(R.id.button1)).setTextColor(this.getResources().getColor(R.color.White)); this.findViewById(R.id.button1).setBackgroundResource(R.drawable.bar); this.findViewById(R.id.scrollView1).setVisibility(View.GONE); this.findViewById(R.id.scrollView3).setVisibility(View.VISIBLE); break;
			case R.id.button1: v.setBackgroundResource(R.drawable.whitebar); ((Button)v).setTextColor(this.getResources().getColor(R.color.Black)); ((Button)this.findViewById(R.id.button2)).setTextColor(this.getResources().getColor(R.color.White)); this.findViewById(R.id.button2).setBackgroundResource(R.drawable.bar); this.findViewById(R.id.scrollView1).setVisibility(View.VISIBLE); this.findViewById(R.id.scrollView3).setVisibility(View.GONE); break;
			case R.id.about_us_menu:
			case R.id.contact_menu:
			case R.id.workshops_menu:
			case R.id.results_menu:
			case R.id.lodging_menu:
			case R.id.travel_menu:
			case R.id.weather_menu:
			case R.id.speakers_menu: ActionBar.setClick(v); this.setResult(1); this.finish();
		}
	}

}
